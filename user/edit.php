<?php

session_start();
if (isset($_SESSION['admin']))
{
    include_once ('menu.php');

?>
<link href="css/style.css" rel="stylesheet" type="text/css" />
 <!--SCRIPTS-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>

<?php

    //include_once('../index.php');
    include_once ('../config/Db.php');
    $conn = (CONNECTION);

    $strSQL = "SELECT * FROM API_USER WHERE USER_ID = '" . $_GET["ID"] . "' ";
    $objParse = oci_parse($conn, $strSQL);
    oci_execute($objParse, OCI_DEFAULT);
    $objResult = oci_fetch_array($objParse);
    $logPeriod = "";
    if ($objResult["LOG_PERIOD"] == "" and $objResult["LOG_PERIOD"] == null)
    {
        $logPeriod = "";
    } else
    {
        $logPeriod = date('Y-m-d', strtotime($objResult["LOG_PERIOD"]));
    }
    if (!$objResult)
    {
        echo "Not found User ID=" . $_GET["USER_ID"];
    } else
    {
    }

?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
<div><h3>User Update Form</h3></div>
<div class="container">
  <form name="frmAdd" method="post">
  <!--User ID-->  
  <div class="row">
    <div class="col-25">
      <label for="userid">User ID</label>
    </div>
    <div class="col-75">
      <input type="text" id="userid" name="txtUserID" value="<?php

    echo $objResult["USER_ID"];

?>" readonly>
    </div>
  </div>
  <!--Password-->  
  <div class="row">
    <div class="col-25">
      <label for="password">Password</label>
    </div>
    <div class="col-75">        
      <input type="password" id="password" name="txtPassword" value="<?php

    echo $objResult["PASSWORD"];

?>" required>
    </div>
  </div>
  <!--Confirm Password-->  
  <div class="row">
    <div class="col-25">
      <label for="password">Confirm Password</label>
    </div>
    <div class="col-75">        
      <input type="password" id="confirm_password" name="txtConfirmPassword" value="<?php

    echo $objResult["PASSWORD"];

?>" required>
    </div>
  </div>
  <!--Client ID-->  
  <?php

    if ($objResult["USER_ID"] != 'admin')
    {

?>
   <div class="row">
    <div class="col-25">
      <label for="clientid">Client ID</label>
    </div>
    <div class="col-72">
    <div class="col-75">
      <input type="text" id="clientid" name="txtClientID" value="<?php

        echo $objResult["CLIENT_ID"];

?>" readonly> 
    </div>
  </div>  
   <!--Log Peroid-->  
  <div class="row">
    <div class="col-25">
      <label for="logperiod">Log Period</label>
    </div>
    <div class="col-75">
        <input type="date" id="logperiod" name="dtLogPeriod" value="<?php

        echo $logPeriod;

?>" >
    </div>
  </div>
  
  <!--Status--> 
  <div class="row">
    <div class="col-25">
      <label for="status">Status</label>
    </div>
    <select name="ddStatus" >
    <?php

        if ($objResult['STATUS'] == 1)
        {
            $sts = 'Active';
        } else
        {
            $sts = 'Inactive';
        }

?> 
    <option value="<?php

        $objResult['STATUS']

?>"><?php

        echo $sts

?></option>    
    <option value="1">Active</option>
    <option value="0">Inactive</option>
</select>
    </div>
  </div> 
  <?php

    }

?>
  
  <div class="row">
    <input type="submit" name="submit" value="Submit">
    <a  href="list.php" type="submit">Cancel</a>
  </div>
  </form>
</div>

<?php

    if (isset($_POST["submit"]))
    {
        include_once ('../config/Db.php');
        $conn = (CONNECTION);

        $user_id = $_POST['txtUserID'];
        $password = $_POST['txtPassword'];
        $confirmPassword = $_POST['txtConfirmPassword'];

        if ($user_id != 'admin')
        {
            $status = $_POST['ddStatus'];
            if ($_POST['dtLogPeriod'] == null)
                $log_peroid = "";
            else
                $log_peroid = date("d-m-Y", strtotime($_POST['dtLogPeriod']));
            if ($_POST['ddStatus'] == null)
            {
                $status = $objResult['STATUS'];
            }
        } else
        {
            $status = 1;
        }

        if ($password == $confirmPassword)
        {
            $strSQL = "UPDATE API_USER SET USER_ID='$user_id',PASSWORD='" . md5($password) .
                "',STATUS='$status',LOG_PERIOD=to_date('" . $log_peroid .
                "','dd-mm-yy') WHERE USER_ID = '$user_id'";
            $objParse = oci_parse($conn, $strSQL);
            $objExecute = oci_execute($objParse, OCI_DEFAULT);

            if ($objExecute)
            {
                oci_commit($conn); //*** Commit Transaction ***//
                echo "Save Done.";
                header('location: list.php');
            } else
            {
                oci_rollback($conn); //*** RollBack Transaction ***//
                echo "Error Save [" . $strSQL . "";
            }
        } else
        {
            echo "Password doesn't match.";
            $message = "Password doesn't match.\\nTry again.";
            echo "<script type='text/javascript'>alert('$message');</script>";

        }
        oci_close($conn);
    }

?>
</body>
</html>
<?php

} else
{
    echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
}

?>