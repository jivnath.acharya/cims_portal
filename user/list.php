<?php
session_start();
if(isset($_SESSION['admin']))
{ 
include_once('menu.php');
?>
<?php
include_once ('../config/Db.php');
$conn = (CONNECTION);
$strSQL     = "SELECT * FROM API_USER ORDER BY upper(USER_ID) ASC, USER_ID";
$objParse   = oci_parse($conn, $strSQL);
oci_execute($objParse, OCI_DEFAULT);
?> 

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="../css/datatables.min.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/datatables.min.js"></script>
<!-- <script src="../css/paging.js"></script> -->
</head>
<body>
<div><h3>User List</h3></div>
<div class="container">  
    <a class="button" href="add.php">Add</a>   
<table id="tableData">
<thead>
  <tr>
    <th>User ID</th>    
    <th>Client ID</th>  
    <th>Loing Date</th> 
    <th>Token Created</th>
    <th>Token Expire</th> 
    <th>Log Peroid</th>   
    <th>Status</th>
    <th>Remark</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
<?php
    while ($objResult = oci_fetch_array($objParse, OCI_ASSOC + OCI_RETURN_NULLS)):
    {  
        if ($objResult["STATUS"] == 1)
        $status = 'Active';
        else
        $status = 'Inactive'
?>  

    <tr>  
    <td><div  align="center"><?php echo $objResult["USER_ID"]; ?></div></td>    
    <td><?php echo $objResult["CLIENT_ID"]; ?></td>   
    <td><?php echo $objResult["LOGIN_DATE"]; ?></td>
    <td><?php echo $objResult["CREATED_DATE"]; ?></td>
    <td><?php echo $objResult["EXPIRE_DATE"]; ?></td>
    <td><?php echo $objResult["LOG_PERIOD"]; ?></td>    
    <td><?php echo $status; ?></td>
    <td><?php echo $objResult["REMARK"]; ?></td>               
    <td align="center"><a  href="edit.php?ID=<?php echo $objResult["USER_ID"];?>" >Edit</a></td>    
    </tr> 
    <?php
    }
    ?> 
    <?php
    endwhile;
    ?> 
  <tbody>
</table>
<script type="text/javascript">
            $(document).ready(function() {
               // $('#tableData').paging({limit:8});
                $('#tableData').DataTable();
            });
        </script>
</div>
</body>
</html>
<?php
}
else{
echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
           }
?>