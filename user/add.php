<?php

session_start();
if (isset($_SESSION['admin']))
{
    include_once ('menu.php');

?>
<?php

    include_once ('../config/Db.php');
    $conn = (CONNECTION);

?><!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
<div><h3>User Add Form</h3></div>
<div class="container">
  <form name="frmAdd" method="post">
  <div class="row">
    <div class="col-25">
      <label for="userid">User ID</label>
    </div>
    <div class="col-75">
      <input type="text" id="userid" name="txtUserID" placeholder="Your user ID.." required>
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="password">Password</label>
    </div>
    <div class="col-75">
      <input type="password" id="password" name="txtPassword" placeholder="Your password.." required>
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="confirmpassword">Confirm Password</label>
    </div>
    <div class="col-75">
      <input type="password" id="confirm_password" name="txtConfirmPassword" placeholder="Your Confirm password.." required>
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="clientid">Client ID</label>
    </div>
    <div class="col-75">
    <select name="ddClientID" required>  
     <option value=""></option>
<?php

    $sql = 'SELECT CLIENT_ID FROM api_organization';
    $stid = oci_parse($conn, $sql);
    oci_execute($stid);

?>
<?php

    while ($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
    {
        echo '<option value="' . $row['CLIENT_ID'] . '">' . $row['CLIENT_ID'] .
            '</option>';
    }

?> 
</select>
      
    </div>
  </div>   
  <div class="row">
    <div class="col-25">
      <label for="logperiod">Log Peroid</label>
    </div>
    <div class="col-75">
      <input type="date" id="logperiod" name="dtLogPeriod">
    </div>
  </div>
  <div class="row">
    <input type="submit" name="submit" value="Submit">
    <a  href="list.php" >Cancel</a>
  </div>
  </form>
</div>

<?php

    if (isset($_POST["submit"]))
    {
        include_once ('../config/Db.php');
        $conn = (CONNECTION);

        $user_id = $_POST['txtUserID'];
        $password = $_POST['txtPassword'];
        $client_id = $_POST['ddClientID'];
        $confirm_password = $_POST['txtConfirmPassword'];
        if ($_POST['dtLogPeriod'] == null)
            $log_peroid = "";
        else
            $log_peroid = date("d-m-Y", strtotime($_POST['dtLogPeriod']));
        $status = '1';
        if ($password == $confirm_password)
        {
            $stid = oci_parse($conn, "SELECT * FROM API_USER WHERE USER_ID = :txtUserID");
            oci_bind_by_name($stid, ':txtUserID', $user_id);
            oci_execute($stid);
            $row = oci_fetch_array($stid, OCI_ASSOC);

            if (!isset($row['USER_ID']))
            {
                $strSQL = "INSERT INTO API_USER (USER_ID,PASSWORD,CLIENT_ID,STATUS,LOG_PERIOD) VALUES('$user_id','" .
                    md5($password) . "','$client_id','$status', to_date('" . $log_peroid .
                    "','dd-mm-yy'))";
                $objParse = oci_parse($conn, $strSQL);
                $objExecute = oci_execute($objParse, OCI_DEFAULT);
                if ($objExecute)
                {
                    oci_commit($conn); //*** Commit Transaction ***//
                    echo "Save Done.";
                    header('location: list.php');
                } else
                {
                    oci_rollback($conn); //*** RollBack Transaction ***//
                    echo "Error Save [" . $strSQL . "";
                }
                oci_close($conn);
            } else
            {
                //header("Location: add1.php?message=User ID already exists.");
                echo "User ID already exists.";
            }
        } else
        {
            echo "Password doesn't match.";
            $message = "Password doesn't match.\\nTry again.";
            echo "<script type='text/javascript'>alert('$message');</script>";

        }
    }

?>
</body>
</html>
<?php

} else
{
    echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
}

?>