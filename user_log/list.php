<?php
session_start();
if(isset($_SESSION['admin']))
{ 
include_once('menu.php');
?>
<?php
include_once ('../config/Db.php');
$conn = (CONNECTION);
$strSQL     = "SELECT * FROM API_LOG ORDER BY LOG_DATE DESC";
$objParse   = oci_parse($conn, $strSQL);
oci_execute($objParse, OCI_DEFAULT);
?> 

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="../css/datatables.min.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"/> 

<script type="text/javascript" src="../js/datatables.min.js"></script>
<script type="text/javascript" src="../js/buttons.print.min.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>


<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>


<!-- <script src="../css/paging.js"></script> -->
<style type="text/css">
    .title{
       width: 100%; 
    }
    .heading{
       width: 95%; 
       float: left;
    }
    .service{
       width: 5%; 
       float: right;
       margin-top: 20px;
    }
</style>
</head>
<body>
<div class="title">    
    <div class="heading">
        <h3>User Log List</h3>
    </div>
    <div class="service">
         <a href="javascript:void(0);" onclick="doPrint()"><img src="../image/print.gif"></a>
        <img src="../image/export.png">
    </div>
</div>
<div class="container" style="margin-bottom: 250px;">
<table id="tableData"  class="display">
    <thead>
      <tr>
        <th>User ID</th>
        <th>Device ID</th>
        <th>IP</th>   
        <th>Citizenship No</th>
        <th>Date of Birth</th>
        <th>Log Date</th>    
        <th>Status</th>
        <th>Remark</th>    
      </tr>
    </thead>
    <tbody>
        <!-- <tr> -->
        <?php
        while ($objResult = oci_fetch_array($objParse, OCI_ASSOC + OCI_RETURN_NULLS)):
        {  
        ?>  
        <tr>  
            <td><div  align="center"><?php echo $objResult["USER_ID"]; ?></div></td> 
            <td><?php echo $objResult["DEVICE_ID"]; ?></td>
            <td><?php echo $objResult["IP"]; ?></td>   
            <td><?php echo $objResult["CITIZENSHIP_NO"]; ?></td>
            <td><?php echo $objResult["DATE_OF_BIRTH"]; ?></td>   
            <td><?php echo $objResult["LOG_DATE"]; ?></td>
            <td><?php echo $objResult["STATUS"]; ?></td>
            <td><?php echo $objResult["REMARK"]; ?></td>
        </tr> 
        <?php
        }
        ?> 
        <?php
        endwhile;
        ?>
        <!-- </tr>   -->
    </tbody>
</table>
<script type="text/javascript">
        $(document).ready(function() {
            //$('#tableData').paging({limit:8});
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            $('#tableData').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        messageTop: '<span style="float:right;" >'+date+'</span>',
                        title : 'User Log'
                    },
                    {
                        extend: 'excelHtml5',
                        title: 'UserLog_'+date
                    }                    
                ]
            });
        });
</script>
</div>
</body>
</html>
<?php
}
else{
echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
           }
?>