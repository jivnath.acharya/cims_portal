<html>
<head>
<!--META-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign In Form</title>

<!--STYLESHEETS-->
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!--SCRIPTS-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<!--Slider-in icons-->
<script type="text/javascript">
$(document).ready(function() {
	$(".username").focus(function() {
		$(".user-icon").css("left","-48px");
	});
	$(".username").blur(function() {
		$(".user-icon").css("left","0px");
	});
	
	$(".password").focus(function() {
		$(".pass-icon").css("left","-48px");
	});
	$(".password").blur(function() {
		$(".pass-icon").css("left","0px");
	});
});
</script>

</head>
<body>

<!--WRAPPER-->
  
<div id="wrapper">
<?php
session_start();


if (!isset($_REQUEST["submit"]))
{

?>  


<!--Sign In FORM-->

<form name="login-form" class="login-form" action="" method="post">

	<!--HEADER-->
    <div class="header">
    <img src="image/logo.png" width="70px" style="float: LEFT;">
    <h1 style="color: #2460b9; PADDING:10PX 80PX;">CIMS</h1>
    <br />
    <!--TITLE--><h2 style="clear: both;">Sign In</h2><!--END TITLE-->
   
    </div>
    <!--END HEADER-->
	
	<!--CONTENT-->
    <div class="content">
	<!--USERNAME--><input name="userid" type="text" class="input username" value="Username" onfocus="this.value=''" /><!--END USERNAME-->
    <!--PASSWORD--><input name="pass" type="password" class="input password" value="Password" onfocus="this.value=''" /><!--END PASSWORD-->
    </div>
    <!--END CONTENT-->
    
    <!--FOOTER-->
    <div class="footer">
    <!--Sign In BUTTON--><input type="submit" name="submit" value="Sign In" class="button" /><!--END Sign In BUTTON-->
    </div>
    <!--END FOOTER-->

</form>
<!--END Sign In FORM-->
	<?php

} else
{
    if (isset($_POST["submit"]))
    {
        
        include_once ('config/Db.php');
        $conn = (CONNECTION);
        $user_id = $_POST['userid'];
        $password = md5($_POST['pass']);     

        $stid = oci_parse($conn, 'SELECT * FROM API_USER WHERE USER_ID = :userid and PASSWORD=:pass and client_id is null');        
        oci_bind_by_name($stid, ':userid', $user_id);
        oci_bind_by_name($stid, ':pass', $password);
        oci_execute($stid);
        $rowcount=oci_fetch_all($stid,$res);   
        if ($rowcount>0)
        {                      
            $_SESSION['admin'] =true;
            echo '
            <script language="JavaScript">
		      window.location = \'organization/list.php\';
            </script>';
            oci_close($conn);
        } else
        {
            $message = "Username and/or Password incorrect.\\nTry again.";
            echo "<script type='text/javascript'>alert('$message');</script>";
            echo '
	       <script language="JavaScript">
		      window.location = \'signin.php\';
	       </script>';
           
        }
    }
}
?>
</div>
<!--END WRAPPER-->

<!--GRADIENT--><div class="gradient"></div><!--END GRADIENT-->

</body>
</html>

  

