

<?php
session_start();
if(isset($_SESSION['admin']))
{ 
include_once('menu.php');
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/main.css">


</head>
<body>
<?php
include_once ('../config/Db.php');
$conn = (CONNECTION);
$strSQL = "SELECT * FROM API_ORGANIZATION WHERE CLIENT_ID = '".$_GET["ID"]."' ";
$objParse = oci_parse ($conn, $strSQL);
oci_execute ($objParse,OCI_DEFAULT);
$objResult = oci_fetch_array($objParse);
if(!$objResult)
{
echo "Not found Client ID=".$_GET["CLIENT_ID"];
}
else
{
 }
?>
<div><h3>Organization Update Form</h3></div>

<div class="container">
  <form name="frmAdd" method="post">
  <div class="row">
    <div class="col-25">
      <label for="clientid">Client ID</label>
    </div>
    <div class="col-75">
      <input type="text" id="clientid" name="txtClientID" value="<?php echo $objResult["CLIENT_ID"];?>" readonly="">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="name">Name</label>
    </div>
    <div class="col-75">
      <input type="text" id="name" name="txtName" value="<?php echo $objResult["NAME"];?>">
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="address">Address</label>
    </div>
    <div class="col-75">
      <input type="text" id="address" name="txtAddress" value="<?php echo $objResult["ADDRESS"];?>">
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="email">Email</label>
    </div>
    <div class="col-75">
      <input type="text" id="email" name="txtEmail" value="<?php echo $objResult["EMAIL"];?>">
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="phone">Phone</label>
    </div>
    <div class="col-75">
      <input type="text" id="phone" name="txtPhone" value="<?php echo $objResult["PHONE"];?>">
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="contactperson">Contact Person</label>
    </div>
    <div class="col-75">
      <input type="text" id="contactperson" name="txtContactPerson" value="<?php echo $objResult["CONTACT_PERSON"];?>">
    </div>
  </div>  
  <div class="row">
    <div class="col-25">
      <label for="remark">Remark</label>
    </div>
    <div class="col-75">
    <input type="text" id="remark" name="txtRemark" value="<?php echo $objResult["REMARK"];?>">      
    </div>
  </div>
  <div class="row">
    <input type="submit" name="submit" value="Submit">
    <a href="list.php" type="submit">Cancel</a>
  </div>
  </form>
</div>
<?php
if (isset($_POST["submit"]))
{   
include_once ('../config/Db.php');
$conn = (CONNECTION);

$strSQL = "UPDATE API_ORGANIZATION SET ";
$strSQL .="CLIENT_ID = '".$_POST["txtClientID"]."' ";
$strSQL .=",NAME = '".$_POST["txtName"]."' ";
$strSQL .=",ADDRESS = '".$_POST["txtAddress"]."' ";
$strSQL .=",EMAIL = '".$_POST["txtEmail"]."' ";
$strSQL .=",PHONE = '".$_POST["txtPhone"]."' ";
$strSQL .=",CONTACT_PERSON = '".$_POST["txtContactPerson"]."' ";
$strSQL .=",REMARK = '".$_POST["txtRemark"]."' ";
$strSQL .="WHERE CLIENT_ID = '".$_GET["ID"]."' ";
$objParse = oci_parse($conn, $strSQL);
$objExecute = oci_execute($objParse, OCI_DEFAULT);

if($objExecute)
{
oci_commit($conn); //*** Commit Transaction ***//
echo "Save Done.";
header('location: list.php');
}
else
{
oci_rollback($conn); //*** RollBack Transaction ***//
echo "Error Save [".$strSQL."";
}
oci_close($conn);
}
?>

</body>
</html>
<?php
}
else{
echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
           }
?>