<?php
session_start();
if(isset($_SESSION['admin']))
{ 
include_once('menu.php');
?>
<?php
include_once ('../config/Db.php');
$conn = (CONNECTION);

    $strSQL     = "SELECT * FROM api_organization ORDER BY upper(NAME) ASC, NAME";
    $objParse   = oci_parse($conn, $strSQL);
    oci_execute($objParse, OCI_DEFAULT);
?> 

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link rel="stylesheet" type="text/css" href="../css/datatables.min.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/datatables.min.js"></script>
<!-- <script src="../css/paging.js"></script> -->
</head>
<body>
<div><h3>Organization List</h3></div>
<div class="container">
    <a class="button" href="add.php" >Add</a>
<table id="tableData">
<thead>
  <tr>
    <th>Client ID</th>
    <th>Name</th>
    <th>Address</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Contact Person</th>
    <th>Remark</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
<?php
    while ($objResult = oci_fetch_array($objParse, OCI_BOTH)):
    {  
        $remark = isset($objResult["REMARK"]);       
        if($remark == null)
        $remark = '';
        else 
        $remark = $objResult["REMARK"];

?>  

             <tr>  
             <td><div  align="center"><?php echo $objResult["CLIENT_ID"]; ?></div></td> 
             <td><?php echo $objResult["NAME"] ?></td>
             <td><?php echo $objResult["ADDRESS"]; ?></td>
             <td><?php echo $objResult["EMAIL"]; ?></td>
             <td><div  align="center"><?php echo $objResult["PHONE"]; ?></div></td>  
             <td align="right"><?php echo $objResult["CONTACT_PERSON"]; ?></td>
             <td align="right"><?php echo $remark; ?></td>  
             <td align="center"><a  href="edit.php?ID=<?php echo $objResult["CLIENT_ID"];?>" >Edit</a></td>    
               </tr> 
               <?php
                }
               ?> 
                 <?php
                endwhile;
               ?>
  </tbody>
</table>
<script type="text/javascript">
            $(document).ready(function() {
                //$('#tableData').paging({limit:8});
                $('#tableData').DataTable();
            });
        </script>
</div>
</body>
</html>
<?php
}
else{
echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
           }
?>