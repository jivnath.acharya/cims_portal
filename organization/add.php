
<?php
session_start();
if(isset($_SESSION['admin']))
{ 
include_once('menu.php');
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
<div><h3>Organization Add Form</h3></div>
<div class="container">

  <form name="frmAdd" method="post">
  <div class="row">
    <div class="col-25">
      <label for="clientid">Client ID</label>
    </div>
    <div class="col-75">
      <input type="text" id="clientid" name="txtClientID" placeholder="Generate your client ID.." required="" readonly="" > 
      <input type="button" name="generate" value="Generate Client ID" onClick="randomString();">&nbsp;   
    </div>    
    
  </div>
  <div class="row">
    <div class="col-25">
      <label for="name">Name</label>
    </div>
    <div class="col-75">
      <input type="text" id="name" name="txtName" placeholder="Your name.." required>
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="address">Address</label>
    </div>
    <div class="col-75">
      <input type="text" id="address" name="txtAddress" placeholder="Your address.." required>
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="email">Email</label>
    </div>
    <div class="col-75">
      <input type="text" id="email" name="txtEmail" placeholder="Your email.." required>
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="phone">Phone</label>
    </div>
    <div class="col-75">
      <input type="text" id="phone" name="txtPhone" placeholder="Your phone number.." MaxLength="15" required>
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="contactperson">Contact person</label>
    </div>
    <div class="col-75">
      <input type="text" id="contactperson" name="txtContactPerson" placeholder="Contact person.." required>
    </div>
  </div>  
  <div class="row">
    <div class="col-25">
      <label for="remark">Remark</label>
    </div>
    <div class="col-75">
    <input type="text" id="remark" name="txtRemark" placeholder="Write something..">      
    </div>
  </div>
  <div class="row">
    <input type="submit" name="submit" value="Submit">
    <a href="list.php" >Cancel</a>
  </div>

  </form>
  
  
</div>


<script language="javascript" type="text/javascript">
function randomString() {
	var chars = "0123456789";
	var string_length = 4;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	document.frmAdd.txtClientID.value = randomstring;
}
</script>

<?php

if (isset($_POST["submit"]))
{
    include_once ('../config/Db.php');
    $conn = (CONNECTION);
    if (!filter_var($_POST["txtEmail"], FILTER_VALIDATE_EMAIL))
    {
        echo "Invalid email format";
        return;
    }
    $clientID = $_POST["txtClientID"];
    $stid = oci_parse($conn,
        "SELECT * FROM API_ORGANIZATION WHERE CLIENT_ID = :txtClientID");
    oci_bind_by_name($stid, ':txtClientID', $clientID);
    oci_execute($stid);
    $row = oci_fetch_array($stid, OCI_ASSOC);
    if ((!isset($row['CLIENT_ID'])))
    {
        $strSQL = "INSERT INTO API_ORGANIZATION ";
        $strSQL .= "(CLIENT_ID,NAME,ADDRESS,EMAIL,PHONE,CONTACT_PERSON,REMARK) ";
        $strSQL .= "VALUES ";
        $strSQL .= "('" . $_POST["txtClientID"] . "','" . $_POST["txtName"] . "','" . $_POST["txtAddress"] .
            "','" . $_POST["txtEmail"] . "',
            '" . $_POST["txtPhone"] . "','" . $_POST["txtContactPerson"] . "','" .
            $_POST["txtRemark"] . "')";
        $objParse = oci_parse($conn, $strSQL);
        $objExecute = oci_execute($objParse, OCI_DEFAULT);

        if ($objExecute)
        {
            oci_commit($conn); //*** Commit Transaction ***//
            echo "Save Done.";
            header('location: list.php');
        } else
        {
            oci_rollback($conn); //*** RollBack Transaction ***//
            echo "Error Save [" . $strSQL . "";
        }
    } else
    {
        //header("Location: add1.php?message=User ID already exists.");
        echo "Client ID already exists.";
    }

    oci_close($conn);
}

?>


</body>
</html>
<?php
}
else{
echo '
	       <script language="JavaScript">
		      window.location = \'../signin.php\';
	       </script>';
           }
?>